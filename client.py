import socket
import json
import tqdm

try:
    with open('data/client.json') as f:
        data = json.loads(f.read())
        name = data.get('name')
        port = data.get('port')
        host = data.get('host')
        gfile = data.get('gfile')
        repeat = data.get('repeat')
        f.close()
except FileNotFoundError:
    print("FILE NOT FOUND")

print(data)

try:
    with open(gfile) as f:
        gcode = f.read().split('\n')
        f.close()
except FileNotFoundError:
    print("FILE {} NOT FOUND".format(gfile))


while gcode[-1] == '':
    gcode.pop()

print('----------')
for _ in tqdm.tqdm(range(int(repeat))):
    for code in tqdm.tqdm(gcode):
        sock = socket.socket()
        #print("SOCKET NEW")

        sock.connect((host, port))
        #print("SOCKET CONNECT")

        sock.send(code.encode())
        print("\nSEND {}".format(code))

        data = sock.recv(1024)
        print("RECEIVE {}".format(data.decode()))
        if data.decode() == '':
            break

        sock.close()
        #print("SOCKET CLOSE")

    print('\n')
    print('----------')
